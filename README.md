# [sensiolabs](https://phppackages.org/s/sensiolabs)/[security-checker](https://phppackages.org/p/sensiolabs/security-checker)

PHP frontend for security.symfony.com https://security.symfony.com

## Bug
* v6.0.0
  * Needs extension ctype
  * Error https://github.com/sensiolabs/security-checker/issues/163
* Does not work then called from "composer global exec", does not print the test result on screen.
  * v6.0.0
  * v5.0.1
  * v4.1.8